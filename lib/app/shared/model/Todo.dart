import 'package:cloud_firestore/cloud_firestore.dart';

class Todo {
  String name;
  bool check;
  DocumentReference reference;

  Todo({this.name = "", this.check = false, this.reference});

  factory Todo.fromDocument(DocumentSnapshot document) {
    return Todo(
        check: document["checked"],
        name: document["title"],
        reference: document.reference);
  }

  Future delete(){
    reference.delete();
  }

  Future save() async {
    if (reference == null) {
      reference = await Firestore.instance.collection("TODO").add({ "title": name, "checked": check });
    } else {
      reference.updateData({"title": name, "checked": check});
    }
  }
}
