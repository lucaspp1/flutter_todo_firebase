import 'package:todo/app/shared/model/Todo.dart';

abstract class ITodoRepository {
  
  Stream<List<Todo>> getTodo();

}