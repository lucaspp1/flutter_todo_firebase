import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:todo/app/shared/model/Todo.dart';
import 'package:todo/app/shared/repositories/todo_repository_interface.dart';

class TodoRepository implements ITodoRepository {
  final Firestore fireStore;

  TodoRepository(this.fireStore);

  @override
  Stream<List<Todo>> getTodo() {
    return fireStore.collection("TODO").snapshots().map((query) {
      return query.documents.map((doc) {
        return Todo.fromDocument(doc);
      }).toList();
    });
  }
}
