import 'package:mobx/mobx.dart';
import 'package:todo/app/shared/model/Todo.dart';
import 'package:todo/app/shared/repositories/todo_repository_interface.dart';

part 'home_controller.g.dart';

class HomeController = _HomeBase with _$HomeController;

abstract class _HomeBase with Store {
  
  final ITodoRepository todoRepository;

  @observable
  ObservableStream<List<Todo>> todoList;

  _HomeBase(ITodoRepository this.todoRepository){
    getList();
  }
  

  @action
  getList(){
    todoList = todoRepository.getTodo().asObservable();
  }
}
