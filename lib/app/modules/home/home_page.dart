import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:todo/app/shared/model/Todo.dart';

import 'home_controller.dart';

class HomePage extends StatefulWidget {
  final String title;
  const HomePage({Key key, this.title = "Home"}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends ModularState<HomePage, HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Observer(builder: (_) {
        if (controller.todoList.hasError) {
          return Center(
              child: RaisedButton(
            onPressed: controller.getList(),
            child: Text("Error"),
          ));
        }

        if (controller.todoList.data == null) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }

        List<Todo> listaTodo = controller.todoList.data;

        return ListView.builder(
            itemCount: listaTodo.length,
            itemBuilder: (_, index) {
              Todo todo = listaTodo[index];
              return ListTile(
                title: Text(todo.name),
                onTap: () {
                  _showDialog(todo);
                },
                leading: IconButton(icon: Icon(Icons.remove_circle_outline, color: Colors.red,), onPressed: () {
                  todo.delete();
                }),
                trailing: Checkbox(
                    value: todo.check,
                    onChanged: (check) {
                      todo.check = check;
                      todo.save();
                    }),
              );
            });
      }),
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            _showDialog();
          },
          child: Icon(Icons.add)),
    );
  }

  _showDialog([Todo todo]) {
    todo ??= Todo();
    showDialog(
      context: context,
      builder: (_) {
        return AlertDialog(
          title: Text(todo.name.isEmpty ? "Adicionar novo" : "Editar item"),
          content: TextFormField(
            initialValue: todo.name,
            onChanged: (value) {
              todo.name = value;
            },
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: "escreva ...",
            ),
          ),
          actions: <Widget>[
            FlatButton(
                onPressed: () {
                  Modular.to.pop();
                },
                child: Text("Cancelar")),
            FlatButton(
                onPressed: () async {
                  await todo.save();
                  Modular.to.pop();
                },
                child: Text("Adicionar")),
          ],
        );
      },
    );
  }
}
